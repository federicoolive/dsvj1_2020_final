#include "loop.h"
#include "config/config.h"

namespace game
{
	namespace loop
	{
		bool isPause = false;
		void init()
		{
			InitWindow(800, 450, "Final");
			SetTargetFPS(60);

			isPause = false;
		}

		void deinit()
		{
			CloseWindow();
			
		}

		void play()
		{
			init();

			while (!WindowShouldClose())
			{
				input();
				update();
				draw();
			}

			deinit();
		}

		void input()
		{
			
		}

		void update()
		{

		}

		void draw()
		{
			BeginDrawing();
			ClearBackground(RAYWHITE);

			DrawText("Congrats! You created your first window!", 190, 200, 20, LIGHTGRAY);

			EndDrawing();
		}
	}
}