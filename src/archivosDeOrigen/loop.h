#ifndef LOOP_H
#define LOOP_H

#include "raylib.h"

namespace game
{
	namespace loop
	{
		void init();
		void deinit();

		void play();

		void input();
		void update();
		void draw();
	}
}

#endif